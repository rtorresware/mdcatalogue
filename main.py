from kivy.app import App
from kivyrt.dataview import DataBox
from kivymd.snackbar import Snackbar


class Catalog(App):
    def notify(self, text):
        Snackbar(text).show()


Catalog().run()

